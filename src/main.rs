extern crate num;
extern crate ansi_term;
use num::{BigUint, ToPrimitive};
use std::convert::TryFrom;
use ansi_term::Colour;

fn main() {
    println!("========================={} {} {}========================", Colour::Red.bold().paint("Coded"), Colour::Green.bold().paint("by"), Colour::Blue.bold().paint("seizetheday"));
    let mut n: Vec<i32> = Vec::new();
    let mut cipher = String::new();

    let (mut nth, e, phi_n);
    let mut d: i32 = 0;
    let mut flag: BigUint;

    //Input validation
    loop {
        println!("=====================================================================");
        println!("Please enter the value of n: ");
        let mut buffer = String::new();
        std::io::stdin()
            .read_line(&mut buffer)
            .expect("Failed to read input.");

        let buffer: i32 = match buffer.trim().parse::<i32>() {
            Ok(parsed_input) => parsed_input,
            Err(_) => continue,
        };
        nth = buffer;
        break;
    }

    //Another input validation
    loop {
        println!("=====================================================================");
        println!("Please enter the value of e: ");
        let mut buffer_two = String::new();
        std::io::stdin()
            .read_line(&mut buffer_two)
            .expect("Failed to read input.");

        let buffer_two: i32 = match buffer_two.trim().parse::<i32>() {
            Ok(parsed_input) => parsed_input,
            Err(_) => continue,
        };
        e = buffer_two;
        break;
    }

    //Takes cipher as input.
    println!("=====================================================================");
    println!("Please enter the cipher: ");
    std::io::stdin().read_line(&mut cipher).expect("input");
    let cipher = cipher
        .trim()
        .split(' ')
        .flat_map(str::parse::<i32>)
        .collect::<Vec<_>>();
    let nth_backup = nth;

    //Calculating two prime factors(p and q), and storing them in a vector
    for i in 2..nth + 1 {
        while nth % i == 0 {
            n.push(i);
            nth = nth / i;
        }
    }
    let nth = nth_backup;
    println!("=====================================================================");
    println!("Prime numbers are {:?}.", n);
    phi_n = (n[0] - 1) * (n[1] - 1);
    println!("=====================================================================");
    println!("The value of phi_n is {}.", phi_n);
    println!("The value of e is {}.", e);
    println!("=====================================================================");

    //Calculates Decryption key 'd'
    for i in 0..e + 1 {
        if (1 + (i * phi_n)) % e == 0 {
            println!("D is calculated by substituting i with {}.", i);
            d = (1 + (i * phi_n)) / e;
            println!("The value of d is: {}.", d);
        }
    }

    //Extracts the flag
    println!("=====================================================================");
    println!("Flag is: ");
    for i in cipher {
        flag = num::pow(BigUint::from(i as u32), d as usize) % BigUint::try_from(nth).unwrap();
        let final_flag = flag.to_u8().unwrap();
        print!("{} ", final_flag as char);
    }
    println!();
    println!("=====================================================================");
}