This RSA decryption calculator solves basic RSA encryption with given supply modulus(N), ciphertext(C), and encryption exponent(E).  
Sample challenge can be like this:  
n = 485, e = 53, c = 153 75 309 310 74 203 208 401 310 371 363 451 125  
After decrypting this, we get flag:  
```
S K Y - T I D L - 8 5 3 7
```
